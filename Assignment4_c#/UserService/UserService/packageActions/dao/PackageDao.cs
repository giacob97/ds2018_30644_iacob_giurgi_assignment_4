﻿using System;
using System.Collections.Generic;
using System.Linq;
using UserService.packageActions.db;

namespace UserService.packageActions.dao
{
    public class PackageDao
    {
        private ContextEntities Context = new ContextEntities();

        public package AddPackage(package p)
        {
            Context.packages.Add(p);
            Context.SaveChanges();
            return p;
        }

        public IList<package> GetPackages()
        {
            return Context.packages.ToList();
        }

        public package UpdatePackage(package p)
        {
            Context.packages.Attach(p);
            Context.Entry(p).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
            return p;
        }

        public package RemovePackage(package p)
        {
            Context.packages.Attach(p);
            Context.packages.Remove(p);
            Context.SaveChanges();
            return p;
        }
    }
}