﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Services;
using UserService.packageActions.db;

namespace UserService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPackageService" in both code and config file together.
    [ServiceContract]
    public interface IPackageService
    {
        [OperationContract]
        [WebMethod]
        package AddPackage(package p);

        [OperationContract]
        [WebMethod]
        package UpdatePackage(package p);

        [OperationContract]
        [WebMethod]
        package RemovePackage(package p);


        [OperationContract]
        [WebMethod]
        IList<package> GetPackages();

    }
}
