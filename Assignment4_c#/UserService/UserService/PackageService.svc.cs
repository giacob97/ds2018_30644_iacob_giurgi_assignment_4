﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Services;
using UserService.packageActions.dao;
using UserService.packageActions.db;

namespace UserService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PackageService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PackageService.svc or PackageService.svc.cs at the Solution Explorer and start debugging.
    
public class PackageService : IPackageService
    {
        private readonly PackageDao _packageDao = new PackageDao();

        public package AddPackage(package p)
        {
            return _packageDao.AddPackage(p);
        }

        public IList<package> GetPackages()
        {
            return _packageDao.GetPackages();
        }

        public package RemovePackage(package p)
        {
            return _packageDao.RemovePackage(p);
        }

        public package UpdatePackage(package p)
        {
            return _packageDao.UpdatePackage(p);
        }
    }
}
