package services;

import javax.xml.ws.Endpoint;

@SuppressWarnings("all")
public class SOAPPublisher {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8081/user", new UserServiceImpl());
        Endpoint.publish("http://localhost:8081/package", new PackageServiceImpl());
        Endpoint.publish("http://localhost:8081/route", new RouteServiceImpl());
    }
}
