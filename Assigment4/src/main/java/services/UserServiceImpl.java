package services;

import dao.UserDAO;
import entities.User;

import javax.jws.WebService;

@WebService(endpointInterface = "services.IUserService")
public class UserServiceImpl implements IUserService {

    private UserDAO userDAO = new UserDAO();

    @Override
    public User addUser(User user) {
        return userDAO.addUser(user);
    }

    @Override
    public int login(String username, String password) {
        return userDAO.login(username,password);
    }
}
