package services;

import entities.Package;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IPackageService {

    @WebMethod
    Package addPackage(Package p);

    @WebMethod
    void removePackage(int idPackage);

    @WebMethod
    Package updatePackage(Package p);

    @WebMethod
    Package[] getPackagesForUser(int idUser);

    @WebMethod
    Package[] getPackages();

    @WebMethod
    Package findByName(String name,int idUser);

    @WebMethod
    Package findById(int idPackage);



}
