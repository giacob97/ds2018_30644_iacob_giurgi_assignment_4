package services;

import dao.RouteDAO;
import entities.Route;

import javax.jws.WebService;

@WebService(endpointInterface = "services.IRouterService")
public class RouteServiceImpl implements IRouterService{

    private RouteDAO routeDAO = new RouteDAO();

    @Override
    public Route addRoute(Route route) {
        return routeDAO.addRoute(route);
    }

    @Override
    public Route updateRoute(Route route) {
        return routeDAO.updateRoute(route);
    }

    @Override
    public Route findRouteForAPackage(int idPackage) {
        return routeDAO.findRouteForAPackage(idPackage);
    }
}
