package services;


import entities.Route;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IRouterService {

    @WebMethod
    Route addRoute(Route route);

    @WebMethod
    Route updateRoute(Route route);

    @WebMethod
    Route findRouteForAPackage(int idPackage);


}
