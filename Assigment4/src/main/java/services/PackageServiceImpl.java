package services;

import dao.PackageDAO;
import entities.Package;

import javax.jws.WebService;
import java.util.ArrayList;

@WebService(endpointInterface = "services.IPackageService")
public class PackageServiceImpl implements IPackageService {


    private PackageDAO packageDAO = new PackageDAO();


    @Override
    public Package addPackage(Package p) {
        return packageDAO.addPackage(p);
    }

    @Override
    public void removePackage(int idPackage) {
        packageDAO.removePackage(idPackage);
    }

    @Override
    public Package updatePackage(Package p) {
        return packageDAO.updatePackage(p);
    }

    @Override
    public Package[] getPackagesForUser(int idUser) {
        ArrayList<Package> packageArrayList = packageDAO.getPackagesForUser(idUser);
        Package[] packages = packageArrayList.toArray(new Package[0]);
        return packages;
    }

    @Override
    public Package[] getPackages() {
        ArrayList<Package> packageArrayList = packageDAO.getPackages();
        Package[] packages = packageArrayList.toArray(new Package[0]);
        return packages;
    }

    @Override
    public Package findByName(String name, int idUser) {
        return packageDAO.findByName(name,idUser);
    }

    @Override
    public Package findById(int idPackage) {
        return packageDAO.findById(idPackage);
    }
}
