package dao;

import entities.Package;
import entities.Route;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("all")
public class PackageDAO {
    private SessionFactory sessionFactory;

    public Package addPackage(Package p){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        int idPackage = (Integer)session.save(p);
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return p;
    }


    public void removePackage(int idPackage){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Package p = (Package) session.get(Package.class,idPackage);
        Route r = new RouteDAO().findRouteForAPackage(idPackage);
        session.delete(r);
        session.delete(p);
        transaction.commit();
        session.close();
        this.sessionFactory.close();
    }

    public Package updatePackage(Package p){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(p);
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return p;
    }

    public ArrayList<Package> getPackagesForUser(int idUser){
        List<Package> userPackages = new ArrayList<>();
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM Package WHERE (sender.id=:idUser) OR (receiver.id=:idUser)");
        query.setParameter("idUser", idUser);
        userPackages= query.list();
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return (ArrayList)userPackages;
    }

    public Package findByName(String name,int idUser){
        Package userPackage = new Package();
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM Package WHERE ((sender.id=:idUser) OR (receiver.id=:idUser)) AND (name=:name)");
        query.setParameter("idUser", idUser);
        query.setParameter("name",name);
        userPackage = (Package)query.uniqueResult();
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return userPackage;
    }

    public Package findById(int idPackage){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM Package WHERE id = :idPackage");
        query.setParameter("idPackage", idPackage);
        Package p = (Package) query.uniqueResult();
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return p;
    }

    public ArrayList<Package> getPackages(){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM Package");
        ArrayList<Package> packageArrayList = (ArrayList<Package>) query.list();
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return packageArrayList;

    }

}
