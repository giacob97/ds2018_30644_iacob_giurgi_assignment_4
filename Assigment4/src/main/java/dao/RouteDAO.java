package dao;

import entities.Route;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@SuppressWarnings("all")
public class RouteDAO {
    private SessionFactory sessionFactory;

    public Route addRoute(Route route){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        int idRoute = (Integer) session.save(route);
        route.setId(idRoute);
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return route;
    }

    public Route updateRoute(Route route){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(route);;
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return route;
    }

    public Route findRouteForAPackage(int idPackage){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("FROM Route WHERE aPackage.id = :idPackage");
        query.setParameter("idPackage", idPackage);
        Route route = (Route) query.uniqueResult();
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return route;
    }

}
