package dao;

import entities.User;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Random;

@SuppressWarnings("all")
public class UserDAO {

    private SessionFactory sessionFactory;

    public User addUser(User user){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        user.setId(new Random().nextInt(10000)+1);
        int idUser = (Integer) session.save(user);
        user.setId(idUser);
        transaction.commit();
        session.close();
        this.sessionFactory.close();
        return user;
    }


    public int login(String username,String password){
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = this.sessionFactory.openSession();
        Transaction tx = null;
        List<User> users = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username", username);
            users= query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        }
        if(users.isEmpty()){
            return -1;
        }else{
            User user = users.get(0);
            if(user.getPassword().equals(password)){
                return user.getType();
            }
            else{
                return -1;
            }
        }
    }

}
