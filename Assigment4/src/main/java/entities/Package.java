package entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="[dbo].[package]")
public class Package{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="id_sender")
    private User sender;

    @JoinColumn(name="id_receiver")
    @ManyToOne
    private User receiver;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="sender_city")
    private String senderCity;

    @Column(name="destination_city")
    private String destinationCity;

    @Column(name="tracking")
    private boolean tracking;

    @OneToMany(mappedBy="aPackage")
    @Transient
    private List<Route> routes= new ArrayList<>();

    public Package(User sender, User receiver, String name, String description, String senderCity, String destinationCity, boolean tracking) {
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
        this.description = description;
        this.senderCity = senderCity;
        this.destinationCity = destinationCity;
        this.tracking = tracking;
    }

    public Package(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSenderCity() {
        return senderCity;
    }

    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public boolean isTracking() {
        return tracking;
    }

    public void setTracking(boolean tracking) {
        this.tracking = tracking;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}
