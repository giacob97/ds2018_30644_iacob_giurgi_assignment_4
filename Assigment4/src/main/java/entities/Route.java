package entities;

import javax.persistence.*;

@Entity
@Table(name="route")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;


    @ManyToOne
    @JoinColumn(name="id_package")
    private Package aPackage;

    @Column(name="entry")
    private String entry;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Package getaPakage() {
        return aPackage;
    }

    public void setaPakage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }
}
