package entities;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="[dbo].[user]")
public class User {


    @Id
    @Column(name="id")
    private int id;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;

    @Column(name="type")
    private int type;

    @OneToMany(mappedBy="sender")
    @XmlTransient
    @Transient
    private List<Package> senderPackages = new ArrayList<>();

    @OneToMany(mappedBy="receiver")
    @XmlTransient
    @Transient
    private List<Package> receiverPackages = new ArrayList<>();


    public User(String username, String password, int type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public User(){

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Package> getSenderPackages() {
        return senderPackages;
    }

    public void setSenderPackages(List<Package> senderPackages) {
        this.senderPackages = senderPackages;
    }


    public List<Package> getReceiverPackages() {
        return receiverPackages;
    }

    public void setReceiverPackages(List<Package> receiverPackages) {
        this.receiverPackages = receiverPackages;
    }
}
