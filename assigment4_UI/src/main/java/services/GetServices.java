package services;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

@SuppressWarnings("all")
public class GetServices {

    public static IPackageService getIPackageService(){
        URL url = null;
        try {
            url = new URL("http://localhost:8081/package?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        QName qname = new QName("http://services/", "PackageServiceImplService");
        Service service = Service.create(url, qname);
        return service.getPort(IPackageService.class);
    }

    public static IRouterService getIRouteService(){
        URL url = null;
        try {
            url = new URL("http://localhost:8081/route?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        QName qname = new QName("http://services/", "RouteServiceImplService");
        Service service = Service.create(url, qname);
        return service.getPort(IRouterService.class);
    }


    public static IUserService getIUserService(){
        URL url = null;
        try {
            url = new URL("http://localhost:8081/user?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        QName qname = new QName("http://services/", "UserServiceImplService");
        Service service = Service.create(url, qname);
        return service.getPort(IUserService.class);
    }

    public static IPackageCService getIPackageCService(){
        URL url = null;
        try {
            url = new URL("http://localhost:54141/WebService.asmx?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        QName qname = new QName("http://tempuri.org/", "WebServiceSoap12");
        Service service = Service.create(url, qname);
        return service.getPort(IPackageCService.class);
    }


}
