package services;

import entities.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IUserService {

    @WebMethod
    User addUser(User user);

    @WebMethod
    int login(String username, String password);

}
