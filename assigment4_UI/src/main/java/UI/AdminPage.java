package UI;

import entities.Route;
import entities.User;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.GetServices;
import services.IPackageService;
import services.IRouterService;
import services.IUserService;
import entities.Package;

@SuppressWarnings("all")
public class AdminPage extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private TextArea textArea;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Registration Form JavaFX Application");
        GridPane gridPane = createRegistrationFormPane();
        GridPane gridPane1 = createsecondGridpane();
        addUIControls(gridPane);
        addSecondUIControls(gridPane1);
        HBox mainLayout = new HBox();
        mainLayout.setPadding(new Insets(0,5,0,0));
        this.textArea = this.getAllPackages();
        mainLayout.getChildren().addAll(gridPane,gridPane1,textArea);
        Scene scene = new Scene(mainLayout, 1500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private TextArea getAllPackages(){
        TextArea textArea = new TextArea();
        textArea.setMaxWidth(500);
        textArea.setMaxHeight(400);
        Package[] packages = GetServices.getIPackageService().getPackages();
        StringBuilder stringBuilder = new StringBuilder();
        for(Package p : packages){
            stringBuilder.append(p.toString() + " \n");
        }
        textArea.setText(stringBuilder.toString());
        return textArea;
    }

    private GridPane createRegistrationFormPane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER_LEFT);
        gridPane.setPadding(new Insets(35, 270, 140, 40));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        return gridPane;
    }


    private GridPane createsecondGridpane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(0, 80, 140, 0));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        return gridPane;
    }

    private void addUIControls(GridPane gridPane) {
        Label headerLabel = new Label("Add a package");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0,0,2,1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(0, 0,0,0));


        Label nameLabel = new Label("Sender : ");
        gridPane.add(nameLabel, 0,1);


        TextField nameField = new TextField();
        nameField.setPrefHeight(40);
        gridPane.add(nameField, 1,1);



        Label emailLabel = new Label("Receiver: ");
        gridPane.add(emailLabel, 0, 2);


        TextField emailField = new TextField();
        emailField.setPrefHeight(40);
        gridPane.add(emailField, 1, 2);


        Label passwordLabel = new Label("Name: ");
        gridPane.add(passwordLabel, 0, 3);


        TextField passwordField = new TextField();
        passwordField.setPrefHeight(40);
        gridPane.add(passwordField, 1, 3);

        Label descriptionLabel = new Label("Description: ");
        gridPane.add(descriptionLabel, 0, 4);


        TextField descriptionField = new TextField();
        passwordField.setPrefHeight(40);
        gridPane.add(descriptionField, 1, 4);

        Label destLabel = new Label("Dest. city: ");
        gridPane.add(destLabel, 0, 5);


        TextField destField = new TextField();
        passwordField.setPrefHeight(40);
        gridPane.add(destField, 1, 5);

        Label senderLabel = new Label("Sender city: ");
        gridPane.add(senderLabel, 0, 6);


        TextField senderField = new TextField();
        passwordField.setPrefHeight(40);
        gridPane.add(senderField, 1, 6);


        Button submitButton = new Button("Add");
        submitButton.setPrefHeight(40);
        submitButton.setDefaultButton(true);
        submitButton.setPrefWidth(100);
        gridPane.add(submitButton, 0, 7, 2, 1);
        GridPane.setHalignment(submitButton, HPos.CENTER);
        GridPane.setMargin(submitButton, new Insets(20, 0,20,0));
        submitButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                IPackageService iPackageService = GetServices.getIPackageService();
                User senderUser = new User(Integer.valueOf(nameField.getText()));
                User receiverUser = new User(Integer.valueOf(emailField.getText()));
                Package p = new Package(senderUser,receiverUser,passwordField.getText(),descriptionField.getText(),senderField.getText(),destField.getText(),false);
                iPackageService.addPackage(p);
                Package[] packages = GetServices.getIPackageService().getPackages();
                StringBuilder stringBuilder = new StringBuilder();
                for(Package pp : packages){
                    stringBuilder.append(pp.toString() + " \n");
                }
                textArea.setText(stringBuilder.toString());
            }
        });
    }


    private void addSecondUIControls(GridPane gridPane) {
        Label headerLabel = new Label("Tracking details");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0,0,2,1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(0, 0,0,0));

        Label idLabel = new Label("Package id: ");
        gridPane.add(idLabel, 0, 1);


        TextField idField = new TextField();
        idField.setPrefHeight(40);
        gridPane.add(idField, 1, 1);


        Label routeLabel = new Label("New Entry: ");
        gridPane.add(routeLabel, 0, 2);


        TextField routeField = new TextField();
        idField.setPrefHeight(40);
        gridPane.add(routeField, 1, 2);


        Button submitButton = new Button("Track");
        submitButton.setPrefHeight(40);
        submitButton.setDefaultButton(true);
        submitButton.setPrefWidth(100);
        gridPane.add(submitButton, 0, 3, 2, 1);
        GridPane.setHalignment(submitButton, HPos.CENTER);
        GridPane.setMargin(submitButton, new Insets(20, 0,0,0));
        final Text actiontarget = new Text();
        gridPane.add(actiontarget, 1, 4);


        submitButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                IPackageService iPackageService = GetServices.getIPackageService();
                IRouterService iRouterService = GetServices.getIRouteService();
                Package updatePackage = iPackageService.findById(Integer.valueOf(idField.getText()));
                if(updatePackage == null){
                    actiontarget.setText("Unavailable package");
                }
                else{
                    updatePackage.setTracking(true);
                    iPackageService.updatePackage(updatePackage);
                    Route route = new Route(updatePackage,"");
                    iRouterService.addRoute(route);
                    Package[] packages = GetServices.getIPackageService().getPackages();
                    StringBuilder stringBuilder = new StringBuilder();
                    for(Package p : packages){
                        stringBuilder.append(p.toString() + " \n");
                    }
                    textArea.setText(stringBuilder.toString());
                }
            }
        });


        Button updateButton = new Button("Update");
        updateButton.setPrefHeight(40);
        updateButton.setDefaultButton(true);
        updateButton.setPrefWidth(100);
        gridPane.add(updateButton, 0, 4, 2, 1);
        GridPane.setHalignment(updateButton, HPos.CENTER);

        updateButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                IRouterService iRouteService = GetServices.getIRouteService();
                Route route = iRouteService.findRouteForAPackage(Integer.valueOf(idField.getText()));
                StringBuilder newEntry = new StringBuilder();
                newEntry.append('(');
                newEntry.append(routeField.getText());
                newEntry.append("),");
                newEntry.append(route.getEntry());
                System.out.println(newEntry.toString());
                if(route==null){
                    actiontarget.setText("Unavailable route for this package");
                }
                else{

                    route.setEntry(newEntry.toString());
                    iRouteService.updateRoute(route);
                    Package[] packages = GetServices.getIPackageService().getPackages();
                    StringBuilder stringBuilder = new StringBuilder();
                    for(Package p : packages){
                        stringBuilder.append(p.toString() + " \n");
                    }
                    textArea.setText(stringBuilder.toString());
                }
            }
        });


        Button deleteButton = new Button("Delete");
        deleteButton.setPrefHeight(40);
        deleteButton.setDefaultButton(true);
        deleteButton.setPrefWidth(100);
        gridPane.add(deleteButton, 0, 5, 2, 1);
        GridPane.setHalignment(deleteButton, HPos.CENTER);

        deleteButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                IPackageService iPackageService = GetServices.getIPackageService();
                iPackageService.removePackage(Integer.valueOf(idField.getText()));
                Package[] packages = GetServices.getIPackageService().getPackages();
                StringBuilder stringBuilder = new StringBuilder();
                for(Package p : packages){
                    stringBuilder.append(p.toString() + " \n");
                }
                textArea.setText(stringBuilder.toString());
            }
        });

    }

}
