package UI;

import entities.Package;
import entities.Route;
import entities.User;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.GetServices;
import services.IPackageService;
import services.IRouterService;

@SuppressWarnings("all")
public class UserPage extends Application {

    public int loggedUser;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Registration Form JavaFX Application");
        GridPane gridPane = createRegistrationFormPane();
        addUIControls(gridPane);
        Scene scene = new Scene(gridPane, 900, 500);
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    private GridPane createRegistrationFormPane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER_LEFT);
        gridPane.setPadding(new Insets(40, 270, 40, 40));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        return gridPane;
    }


    private void addUIControls(GridPane gridPane) {
        Label headerLabel = new Label("My packages details");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0,0,2,1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(0, 0,0,0));
        Label idLabel = new Label("Package id: ");
        gridPane.add(idLabel, 0, 2);
        TextField idField = new TextField();
        idField.setPrefHeight(40);
        gridPane.add(idField, 0, 3);
        TextArea textArea = new TextArea();
        gridPane.add(textArea,0,1);
        Button submitButton = new Button("Search");
        submitButton.setPrefHeight(40);
        submitButton.setDefaultButton(true);
        submitButton.setPrefWidth(100);
        gridPane.add(submitButton, 0, 4, 2, 1);
        GridPane.setHalignment(submitButton, HPos.CENTER);
        GridPane.setMargin(submitButton, new Insets(20, 0,0,0));

        Button viewButton = new Button("View all");
        viewButton.setPrefHeight(40);
        viewButton.setDefaultButton(true);
        viewButton.setPrefWidth(100);
        gridPane.add(viewButton, 0, 5, 2, 1);
        GridPane.setHalignment(viewButton, HPos.CENTER);
        final Text actiontarget = new Text();
        gridPane.add(actiontarget, 1, 4);
        Package[] packages = GetServices.getIPackageService().getPackagesForUser(1);
        StringBuilder stringBuilder = new StringBuilder();
        for(Package p : packages){
            stringBuilder.append(p.toString() + " \n");
        }
        textArea.setText(stringBuilder.toString());
        submitButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                IPackageService iPackageService = GetServices.getIPackageService();
                IRouterService iRouterService = GetServices.getIRouteService();
                Package updatePackage = iPackageService.findById(Integer.valueOf(idField.getText()));
                StringBuilder sb = new StringBuilder();
                sb.append(updatePackage.toString() + " \n");
                Route route = iRouterService.findRouteForAPackage(updatePackage.getId());
                sb.append("Ruta" + route.getEntry());
                textArea.setText(sb.toString());

            }
        });

        viewButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                Package[] packages = GetServices.getIPackageService().getPackagesForUser(1);
                StringBuilder stringBuilder = new StringBuilder();
                for(Package p : packages){
                    stringBuilder.append(p.toString() + " \n");
                }
                textArea.setText(stringBuilder.toString());
            }
        });
    }

}
