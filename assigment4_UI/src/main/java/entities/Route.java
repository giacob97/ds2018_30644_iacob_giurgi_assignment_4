package entities;
import java.io.Serializable;


public class Route implements Serializable {

    private int id;
    private Package aPackage;
    private String entry;

    public Route(Package aPackage, String entry) {
        this.aPackage = aPackage;
        this.entry = entry;
    }

    public Route(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Package getaPakage() {
        return aPackage;
    }

    public void setaPakage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", aPackage=" + aPackage +
                ", entry='" + entry + '\'' +
                '}';
    }
}
