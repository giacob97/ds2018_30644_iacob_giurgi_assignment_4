package entities;

import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User {

    private int id;
    private String username;
    private String password;
    private int type;
    private List<Package> senderPackages = new ArrayList<>();
    private List<Package> receiverPackages = new ArrayList<>();

    public User(String username, String password, int type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public User(){

    }


    public User(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Package> getSenderPackages() {
        return senderPackages;
    }

    public void setSenderPackages(List<Package> senderPackages) {
        this.senderPackages = senderPackages;
    }

    public List<Package> getReceiverPackages() {
        return receiverPackages;
    }

    public void setReceiverPackages(List<Package> receiverPackages) {
        this.receiverPackages = receiverPackages;
    }
}
